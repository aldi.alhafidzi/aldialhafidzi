import Blanklayout from "@/layouts/blank-layout";

const Quotation = ({}) => {
  return (
    <div className=" m-auto text-xs">
      <section className="mt-0 px-8">
        <h1 className="font-bold text-3xl text-center text-green-1 mb-16">
          Freelance Web Developer Quote
        </h1>

        <div className="grid grid-cols-3 text-xs font-light mb-8">
          <div>
            <h5 className="font-bold">Quotation No</h5>
            <p>0006 / FW02032022</p>
          </div>
          <div>
            <h5 className="font-bold">Issue Date</h5>
            <p>02 Maret 2022</p>
          </div>
          <div>
            <h5 className="font-bold">Validity</h5>
            <p>01 April 2022</p>
          </div>
        </div>

        <h3 className="text-green-1 font-bold border-b border-green-1 pb-2 mb-4 w-1/4 inline-block">
          Company Details
        </h3>

        <div className="grid grid-cols-12 mb-1 gap-2">
          <div className="col-span-4">
            Name <span className="float-right">:</span>
          </div>
          <div className="col-span-8">
            {" "}
            PT Wira Pamungkas Pariwara (Wunderman Thompson Indonesia)
          </div>
        </div>

        <div className="grid grid-cols-12 mb-1 gap-2">
          <div className="col-span-4">
            Address <span className="float-right">:</span>
          </div>
          <div className="col-span-8">
            Menara Sentraya Fl. 26th Jl. Iskandarsyah Raya No. 1A Melawai,
            Jakarta 12160 Indonesia
          </div>
        </div>

        <div className="grid grid-cols-12 mb-1 gap-2">
          <div className="col-span-4">
            Contact Number <span className="float-right">:</span>
          </div>
          <div className="col-span-8">+62 857-1439-0634</div>
        </div>

        <div className="grid grid-cols-12 mb-8 gap-2">
          <div className="col-span-4">
            E-mail <span className="float-right">:</span>
          </div>
          <div className="col-span-8">
            benedict.nathanael@wundermanthompson.com
          </div>
        </div>

        <h3 className="text-green-1 font-bold border-b border-green-1 pb-2 mb-4 w-1/4 inline-block">
          Freelance Details
        </h3>

        <div className="grid grid-cols-12 mb-1 gap-2">
          <div className="col-span-4">
            Name <span className="float-right">:</span>
          </div>
          <div className="col-span-8"> Aldi Alhafidzi</div>
        </div>

        <div className="grid grid-cols-12 mb-1 gap-2">
          <div className="col-span-4">
            Address <span className="float-right">:</span>
          </div>
          <div className="col-span-8">
            Jl. Gunung Puntang, Kp. Sirnagalih 001/006, Pasirmulya, Banjaran,
            Bandung, 40377
          </div>
        </div>

        <div className="grid grid-cols-12 mb-1 gap-2">
          <div className="col-span-4">
            Contact Number <span className="float-right">:</span>
          </div>
          <div className="col-span-8"> +62 822-1603-7320</div>
        </div>

        <div className="grid grid-cols-12 mb-1 gap-2">
          <div className="col-span-4">
            E-mail <span className="float-right">:</span>
          </div>
          <div className="col-span-8"> aldi.alhafidzi@gmail.com</div>
        </div>
        <div className="grid grid-cols-12 mb-1 gap-2">
          <div className="col-span-4">
            NPWP <span className="float-right">:</span>
          </div>
          <div className="col-span-8"> 96.599.575.6-445.000</div>
        </div>
      </section>

      <section className="my-8 pb-4">
        <table className="w-full mb-4">
          <thead className="font-bold text-sm">
            <tr className="border-b bg-green-1 bg-opacity-20">
              <td className="border p-2 py-4 text-center">No</td>
              <td className="border p-2 py-4">Service</td>
              <td className="border p-2 py-4 text-center">Month</td>
              <td className="border p-2 py-4">Per. Month Cost</td>
              <td className="border p-2 py-4">Sub. Total</td>
            </tr>
          </thead>
          <tbody>
            <tr className="border-b">
              <td className="border p-2 text-center">1</td>
              <td className="border p-2">
                Freelance Web Developer <br />
                <br />
                <ul className="list-disc ml-4">
                  <li>Dancow ADKSS</li>
                  <li>Dancow Ramadhan</li>
                </ul>
              </td>
              <td className="border p-2 text-center">1</td>
              <td className="border p-2">Rp 8.000.000</td>
              <td className="border p-2">Rp 8.000.000</td>
            </tr>
            <tr className="border-b">
              <td className="border p-2 text-center">&nbsp;</td>
              <td className="border p-2">&nbsp;</td>
              <td className="border p-2">&nbsp;</td>
              <td className="border p-2">&nbsp;</td>
              <td className="border p-2">&nbsp;</td>
            </tr>
            <tr className="font-bold text-sm">
              <td className="p-2 text-center">&nbsp;</td>
              <td className="p-2">&nbsp;</td>
              <td className="p-2">&nbsp;</td>
              <td className="border p-2 text-center">Total</td>
              <td className="border p-2">Rp. 8.000.000</td>
            </tr>
          </tbody>
        </table>

        <p className="text-left text-xs mb-4">
          Penjelasan biaya diatas belum termasuk PPH.
          <br /> Pembayaran bisa melalui (BCA 3370484090 - Aldi Alhafidzi)
          <br />
          <br />
          <br />
        </p>

        <p className="text-right text-sm italic underline">
          Terimakasih atas pertimbangannya.
        </p>
        <p>
          <b> Bandung, 2 Maret 2022</b>
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <b>Aldi Alhafidzi</b>
        </p>
      </section>

      {/* <section className="my-16 flex items-center justify-center">
        <div className="p-6 border-2 border-black">
          <p>
            <b>To:</b>
          </p>
          <p>
            <b>Wempy Prasetya Markus</b>
          </p>
          <p className="mb-2">
            <b>+62 811-2241-212</b>
          </p>

          <p>
            PT. Maleo Kreatif Indonesia <br />
            Tokopedia Tower
            <br />
            Jl. Prof. DR. Satrio No.Kav 11
            <br />
            RT.3/RW.3 South Jakarta 12950
          </p>
        </div>
      </section> */}
    </div>
  );
};

Quotation.getLayout = function getLayout(page) {
  return <Blanklayout>{page}</Blanklayout>;
};

export default Quotation;
