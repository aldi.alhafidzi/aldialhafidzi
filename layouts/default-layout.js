import Head from "next/head";
import DefaultHeader from "@/components/headers/default-header";
import DefaultFooter from "@/components/footers/default-footer";

export default function Defaultlayout({ children }) {
  return (
    <>
      <Head>
        <title>Aldi Alhafidzi</title>
        <meta name="description" content="Aldi Alhafidzi" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <DefaultHeader />

      <main>{children}</main>

      <DefaultFooter />
    </>
  );
}
