import LogoFacebok from "@/public/assets/images/logo/facebook.png";
import LogoInstagram from "@/public/assets/images/logo/instagram.png";
import LogoYoutube from "@/public/assets/images/logo/youtube.png";
import LogoTwitter from "@/public/assets/images/logo/twitter.png";
import LogoLinkedIn from "@/public/assets/images/logo/linkedin.png";
import LogoGithub from "@/public/assets/images/logo/github.png";
import LogoGitlab from "@/public/assets/images/logo/gitlab.png";
import LogoGmail from "@/public/assets/images/logo/gmail.png";

export const ListLink = [
  {
    link: "https://web.facebook.com/alhafidzi.aldi",
    title: "Facebook",
    image: {
      src: LogoFacebok,
      alt: "Logo Facebook",
    },
  },
  {
    link: "https://www.instagram.com/aldialhafidzi/",
    title: "Instagram",
    image: {
      src: LogoInstagram,
      alt: "Logo Instagram",
    },
  },
  {
    link: "https://www.youtube.com/channel/UC_Z9U50YLT3DV4AtpAwxCnw",
    title: "YouTube",
    image: {
      src: LogoYoutube,
      alt: "Logo Youtube",
    },
  },
  {
    link: "https://twitter.com/aldi_alhafidzi",
    title: "Twitter",
    image: {
      src: LogoTwitter,
      alt: "Logo Twitter",
    },
  },
  {
    link: "https://www.linkedin.com/in/aldi-alhafidzi-981184167/",
    title: "LinkedIn",
    image: {
      src: LogoLinkedIn,
      alt: "Logo LinkedIn",
    },
  },
  {
    link: "https://github.com/aldialhafidzi",
    title: "GitHub",
    image: {
      src: LogoGithub,
      alt: "Logo Github",
    },
  },
  {
    link: "https://gitlab.com/aldi.alhafidzi",
    title: "GitLab",
    image: {
      src: LogoGitlab,
      alt: "Logo Gitlab",
    },
  },
  {
    link: "mailto:aldi.alhafidzi@gmail.com",
    title: "E-mail",
    image: {
      src: LogoGmail,
      alt: "Logo Gmail",
    },
  },
];
