import LogoPina from "@/public/assets/images/logo/pina.jpg";
import LogoWT from "@/public/assets/images/logo/wt.png";
import LogoMaleo from "@/public/assets/images/logo/maleo.png";
import LogoMIRUM from "@/public/assets/images/logo/mirum.jpeg";

export const ListExperience = [
  {
    companyName: "Pina Indonesia",
    jobTitle: "Full Time Web Developer",
    jobDescription: `After I had a little experience in a previous company as a freelance web developer, I joined Pina after receiving an offer from <a class="hover:text-orange-1 font-bold" href="https://id.linkedin.com/in/fajar-kuntoro-35860354" target="_blank">Kang Fajar</a>. In this position I met great new people, got a new atmosphere and new experience in terms of work and knowledge side of stock trading, investing and reksadana. In this company I handle the <a class="hover:text-orange-1 font-bold" href="https://pina.id" target="_blank">Pina Indonesia</a> website and its portal website.`,
    time: "1 June 2021 - this time.",
    logo: LogoPina,
  },
  {
    jobTitle: "Freelance Full Time Web Developer",
    companyName: "Wunderman Thompson",
    jobDescription: `After 7 months freelance part time as a web developer. In this position I learned a lot and handled several projects or campaigns from Nestle brands such as Milo, Dancow, Cerelac, Lactogrow. And I have handled projects from Bank BRI clients, namely Digital Signature which is integrated with privyId, Jatis, BRI LOS and OVO.`,
    time: "1 April 2020 - this time.",
    logo: LogoWT,
  },
  {
    jobTitle: "Freelance Full Time Web Developer",
    companyName: "Maleo Agency",
    jobDescription: `In this company I handle microsites from the silverqueen brand, namely <a class="hover:text-orange-1 font-bold" href="https://banyakmaknacinta.com" targe="_blank">#banyakmaknacinta</a> for 2 months.`,
    time: "3 January 2022 - 3 March 2022",
    logo: LogoMaleo,
  },
  {
    companyName: "Wunderman Thompson",
    jobTitle: "Freelance Part Time Web Developer",
    jobDescription: `After I finished my internship, I was offered a freelance part time job by <a class="hover:text-orange-1 font-bold" href="https://id.linkedin.com/in/andriansandi" targe="_blank">Kang Sandi</a> for 7 months to help work at Mirum which has now been acquired by Wunderman Thompson.`,
    time: "1 September 2019 - 31 March 2020.",
    logo: LogoWT,
  },
  {
    companyName: "Mirum Agency",
    jobTitle: "Internship Web Developer",
    jobDescription: `I started my internship at Mirum Agency for 3 months, the head office is in Jakarta and the Technical Team is on Jl. Lembong No. 8 Bandung. In this company I learned a lot of things from work environment and basic skills for Web Developer such as Git Version Control, Slicing design from Figma or Photoshop to HTML, data cleansing, deployment, SEO and many more.`,
    time: "15 June - 15 August 2019.",
    logo: LogoMIRUM,
  },
];
