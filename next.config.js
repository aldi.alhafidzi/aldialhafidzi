module.exports = {
  reactStrictMode: true,
  trailingSlash: true,
  exportPathMap: async function (
    defaultPathMap,
    { dev, dir, outDir, distDir, buildId }
  ) {
    return {
      "/": { page: "/" },
      "/invoice": { page: "/invoice" },
      "/quotation": { page: "/quotation" }
    };
  },
  // images: {
  //   domains: ['aldialhafidzi.vercel.app'],
  //   loader: 'imgix',
  //   path: 'https://aldialhafidzi.vercel.app/'
  // },
};
